/**
 * @file output.h
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es implementiert alle Ausgaben
 */

#ifndef _OUTPUT_H
#define _OUTPUT_H

#include <stdbool.h>

/*
 ****************************************************************************************
 *  @brief Dies Funktion initialisiert das TFT Display.
 *         Auf dem TFT Display wird entweder eine Fenster f�r die Fehlermeldungen
 *         oder die Stunden als "gro�e" * dargestellt .
 *
 ****************************************************************************************/
void initOutput(void);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt den aktuellen Fehlercode aus.
 *         Liegt kein Fehler vor, wird keine Ausgabe erzeugt.
 *
 ****************************************************************************************/
void displayError(void);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt die als Parameter �bergebene Anzahl der Stunden 
 *         auf dem TFT Display aus. Zuvor wir der Bildschirm gel�scht.
 *         F�r jede Stunde wird ein * ausgegeben. In einer Zeile stehen bis zu 5 Sternen.
 *  @param hrs        Anzahl der Stunden, die ausgegeben werden sollen.
 *  @param displayOn  Dieser Parameter legt fest, ob eine Ausgabe auf dem Bildschirm
                      erzeugt wird.
 *                    (false -> keine Zeichen auf dem Bildschirm)
 *
 ****************************************************************************************/
void displayHrs(unsigned char hrs, bool displayOn);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt die als Parameter �bergebene Anzahl der Minuten 
 *         auf den entsprechenden LEDs bin�r aus. 
 *  @param mins       Anzahl der Minuten, die ausgegeben werden sollen.
 *  @param displayOn  Dieser Parameter legt fest, ob eine Ausgabe auf den LEDs
                      erzeugt wird.
 *                    (false -> alle LEDs aus)
 *
 ****************************************************************************************/
void displayMin(unsigned char mins, bool displayOn);

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt die als Parameter �bergebene Anzahl der Sekunden 
 *         auf den entsprechenden LEDs bin�r aus. 
 *  @param secs       Anzahl der Sekunden, die ausgegeben werden sollen.
 *  @param displayOn  Dieser Parameter legt fest, ob eine Ausgabe auf den LEDs
                      erzeugt wird.
 *                    (false -> alle LEDs aus)
 *
 ****************************************************************************************/
 void displaySec(unsigned char secs, bool displayOn);

#endif /* _OUTPUT_H */

// EOF
