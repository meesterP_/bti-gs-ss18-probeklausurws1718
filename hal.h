/**
 * @file hal.h
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es implementiert den Zugriff auf die GPIOs.
 *        Dabei wird von ausgegangen, dass die verwendeten Ports schon in 
 *        der durch die Funktion Init_TI_Board() gemaess ihrer Funktion als
 *        Input oder Output eingestellt wurden.
 */

#ifndef _HAL_H
#define _HAL_H

#include <stdint.h>
#include <stdbool.h>
#include "TI_memory_map.h"

/**
 * @brief Diese Funktion setzt mehrere nebeneinander liegende Pins eines
 *        Output Ports
 *
 * @param port Das Port, dessen Pins gesetzt werden sollen.
 * @param relevantPins In dieser Maske sind die Pins, die modifiziert 
 *        werden sollen, auf 1 gesetzt.
 * @param mask Diese Maske beschreibt die Werte der zu modifizierenden Pins. 
 */
void setOutputOfPort(GPIO_TypeDef* port, uint16_t relevantPins, uint16_t mask);

/**
 * @brief Diese Funktion liest einen Pin ein, an den ein Druckschalter
 *        angeschlossen ist. �ber einen einfachen Delay Mechanismus 
 *        wird eine softwaremaessige Entprellung realisiert.
 *        Im Fehlerfall wird der entsprechende Error Code gesetzt.
 * @param port Das Port, an dem der Druckschalter angeschlossen ist.
 * @param pin Nummer des Pins, an den der auszulesende Druckschalter angeschlossen ist.
 * @retval Die Funktion liefert true, genau dann wenn der Schalter gedrueckt ist.
 */
bool readButton(GPIO_TypeDef* port, unsigned char pin);

#endif
// EOF
