/**
 * @file mytimer.c
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es erg�nzt das Timer Modul des TI Board um eine Sleep Funktion.
 */

#include "timer.h"
#include "mytimer.h"

/**
 * @brief  Diese Funktion berechnet die Zeitspanne zwischen zwei
 *         Zeitstempeln
 * @param  firstTimeStamp ist der erste (fr�here) Zeitstempel
 * @param  secondTimeStamp ist der zweite (sp�tere) Zeitstempel
 * @retval Die Zeit (in us), die zwischen den beiden Zeitstempeln vergangen ist.
 */
uint32_t timerDiffToUsec(uint32_t firstTimeStamp, uint32_t secondTimeStamp) {
    return (secondTimeStamp - firstTimeStamp) / CONVERT2US;
}

/**
 * @brief Diese Funktion wartet die vorgegebenen Zeitspannen.
 *        Sie ist durch polling der Timers realisiert.
 * @param t Die Zeit in us, die gewartet werden soll. Der Wertebereich des 
 *        Parameters ist 0 .. (UINT32_MAX-1)/TIM2_CLOCK
 */
void sleep(const uint32_t t) {
    uint32_t start_time = getTimeStamp();
    while(timerDiffToUsec(start_time, getTimeStamp()) < t) {}
}

// EOF
