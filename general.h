/**
 * @file general.h
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es stellt die grundlegenden Konstanten und IO Einstellungen bereit.
 */

#ifndef _GENERAL_H
#define _GENERAL_H

#include "TI_memory_map.h"

// Blink Delay der Anzeige in Mikrosekunden
#define BLINK_DELAY					(400 * 1000)


#define BUTTON_PORT        GPIOE

#define PIN_SET_MODE				7
#define PIN_UP							6

#define LED_PORT   					GPIOG

// 6 LEDs zur Darstellung der Minuten
#define FIRST_LED_MINS			8
#define LED_MINS_MASK				(0x003F << FIRST_LED_MINS)

// 6 LEDs zur Darstellung der Sekunden
#define FIRST_LED_SECS			0
#define LED_SECS_MASK				(0x003F << FIRST_LED_SECS)

#endif
// EOF
