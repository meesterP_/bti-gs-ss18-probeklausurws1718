/**
 * @file clock.c
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es implementiert die Funktionen der Uhr.
 */

#include <stdint.h>
#include "TI_Lib.h"
#include "timer.h"
#include "mytimer.h"
#include "general.h"
#include "myerr.h"
#include "output.h"
#include "clock.h"

#define SECONDS_IN_MINUTE 60
#define SECONDS_IN_HOUR 60*SECONDS_IN_MINUTE
#define MICROSEONDS_IN_SECOND 1000000

// Globale Variablen zur Verwaltung der Uhr
uint64_t time = 0;
uint64_t current_timestamp_64 = 0;
uint32_t last_timestamp = 0;
uint32_t overflow_count = 0;
bool blinked = false;

/**
 * @brief  Diese Funktion initialisiert die Uhr
 * @param  None
 * @retval None
 */
void initClock(void) {
    timerinit();
    
    time = 0;
    last_timestamp = 0;
    blinked = false;
}

/**
 * @brief  Die Funktion addiert hrs Stunden auf die aktuelle Uhrzeit.
 * @param  hrs Anzahl der Stunden, die auf die Uhrzeit addiert werden.
 * @retval None
 */
void addStd(unsigned char hrs) {
    time += hrs * SECONDS_IN_HOUR;
}
/**
 * @brief  Die Funktion addiert mins Minuten auf die aktuelle Uhrzeit.
 * @param  mins Anzahl der Minuten, die auf die Uhrzeit addiert werden.
 * @retval None
 */
void addMin(unsigned char mins) {
    time += mins * SECONDS_IN_MINUTE;
}

/**
 * @brief  Die Funktion addiert secs Sekunden auf die aktuelle Uhrzeit.
 * @param  secs Anzahl der Sekunden, die auf die Uhrzeit addiert werden.
 * @retval None
 */
void addSec(unsigned char secs) {
    time += secs;
}

/**
 * @brief  Diese Funktion aktualisiert die Uhr.
 * @param  restart Dieser Parameter gibt an, die Uhr neu gestartet werden soll,
 *         nachdem sie �ber eine l�ngere Zeit nicht aktualisiert wurde.
 *         Dies sollte nach dem Stellen der Uhr durchgef�hrt werden. Ansonsten
 *         wird die Zeit zum Stellen der Uhr auf die Uhrzeit addiert.
 * @retval None
 */

void updateClock(bool restart) {
    if (restart) {
        initClock();
    } else {
        /*uint32_t current_timestamp = getTimeStamp();
        uint32_t current_time = 0;
        
        if (current_timestamp < last_timestamp) { // if that happens we had a timestamp overflow
            uint32_t new_current_timestamp = UINT32_MAX - last_timestamp + current_timestamp; // add the difference between the last_timestamp and max to the current_timestap as new value
            current_time = timerDiffToUsec(0, new_current_timestamp) / 1000000;
            
        } else {
            current_time = timerDiffToUsec(last_timestamp, current_timestamp) / MICROSEONDS_IN_SECOND;
        }*/
        static bool overflow_set = false;
        uint32_t current_timestamp = getTimeStamp();
        if (last_timestamp > current_timestamp && !overflow_set){
            overflow_count++;
            overflow_set = true;
        }
        uint64_t cur_time64 = current_timestamp;
        cur_time64 = (cur_time64<<32)|overflow_count;
    
        uint32_t current_time = (uint32_t) ((cur_time64/(MICROSEONDS_IN_SECOND * CONVERT2US)) >> 32);
        
        
        if (current_time > 0) { // only add time and update last_timestamp if atleast one second has gone by
            time += current_time;
            last_timestamp = current_timestamp;
            overflow_set = false;
        }
        
        // workaround for now with get_uptime()
        /*uint32_t current_timestamp = get_uptime();
        if (current_timestamp >= 1000 + last_timestamp) {
            time += (current_timestamp - last_timestamp) / 1000;
            last_timestamp = current_timestamp;
        }*/
    }
}

/**
 * @brief  Diese Funktion gibt die Uhrzeit auf dem Display und den LEDs aus.
 * @param  blink Diese Parameter legt fest, ob die Darstellung der Uhrzeit blinken
 *         soll. Dies wird beim Einstellen der Uhr verwendet.
 * @retval none
 */
void displayClock(bool blink) {
    unsigned char hours = 0;
    unsigned char minutes = 0;
    uint64_t temp_time = time;
    
    hours = temp_time / (SECONDS_IN_HOUR);
    temp_time -= hours * SECONDS_IN_HOUR;
        
    minutes = temp_time / (SECONDS_IN_MINUTE);
    temp_time -= minutes * SECONDS_IN_MINUTE;
    
    if (hours > 23) {
        hours %= 24;
    }
    
    if (blink) {
        if (timerDiffToUsec(last_timestamp, getTimeStamp()) >= BLINK_DELAY) {
            blinked = !blinked;
        }
        displayHrs(hours, blinked);
        displayMin(minutes, blinked);
        displaySec(temp_time, blinked);
    } else {
        displayHrs(hours, true);
        displayMin(minutes, true);
        displaySec(temp_time, true);
    }
}


// EOF
