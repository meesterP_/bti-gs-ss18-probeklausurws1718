/**
  ******************************************************************************
  * @file    main.c 
  * @author  IHR NAME
  * @date    Dezember 2017
  * @brief   Dieses Modul gehört zur Lösung der GS Probeklausur WS 2017-18. 
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "timer.h"
#include "mytimer.h"

#include "TI_lib.h"
#include "general.h"
#include "hal.h"
#include "output.h"
#include "clock.h"
#include "myerr.h"

/* Funktionen zum Test der einzelnen Teilaufgaben ----------------------------*/

void testA3(void) {
	 Init_TI_Board();
	 timerinit();
	
	 // Warten auf Tastedruck S7
	 while (! readButton(BUTTON_PORT,PIN_SET_MODE)) { /* aktives Warten */ }
	
	 uint32_t t = 4*1000*1000;
	 
	 // LEDs im 5 Sekunden Takt anschalten
   setOutputOfPort(LED_PORT, 0xFFFF, 0x1);
	 sleep(t);	
   setOutputOfPort(LED_PORT, 0x2, 0xFFFF);
	 sleep(t);		 
   setOutputOfPort(LED_PORT, 0x7, 0x7);
	 sleep(t);	
   setOutputOfPort(LED_PORT, 0xFFFF, 0xF);
	 sleep(t);	
   setOutputOfPort(LED_PORT, 0x0010, 0x10);

	 // Auf Knopfdruck warten, dann Lauflicht im Sekundentakt
	 while (! readButton(BUTTON_PORT,PIN_UP)) { /* aktives Warten */ }
   setOutputOfPort(LED_PORT, 0xFFFF, 0x0000);
	 uint16_t h = 0x0001;
	 t = 1 * 1000 * 1000;
	 while (readButton(BUTTON_PORT,PIN_UP)) {
      setOutputOfPort(LED_PORT, 0xFFFF, h);
	    sleep(t);	
		  h = h << 1;
		  if (h == 0) { h = 0x0001; }
	 }
	 
	 // Ausabe einer 3 auf den LEDs
	 setOutputOfPort(LED_PORT, 0xFFFF, timerDiffToUsec(1034 * 84, 1037 * 84));
}

static void waitButton7Pressed(void) {
	 // Warten auf Tastedruck S7
	 while (! readButton(BUTTON_PORT,PIN_SET_MODE)) { /* aktives Warten */ }
	 while (  readButton(BUTTON_PORT,PIN_SET_MODE)) { /* aktives Warten */ }	
}

void testA4(void) {
	 Init_TI_Board();
	 timerinit();
	
	 initOutput();
	 waitButton7Pressed();		displayHrs(12, true); 									// ´Stelle 12 Stunden dar
	 waitButton7Pressed();		displayHrs(1, true);										// ´Stelle 1 Stunde dar
	 waitButton7Pressed();		displayHrs(0, true); 										// ´Stelle 0 Stunden dar
	 waitButton7Pressed();		displayHrs(3, true);										// ´Stelle 3 Stunden dar
	 waitButton7Pressed();		displayHrs(3, false);										// TFT aus
	 waitButton7Pressed();		displayHrs(23, true);										// 23 Stunden
	 waitButton7Pressed();		setError(INTERNAL_ERR);	displayError();	// Fehlercode Ausgabe 
	 waitButton7Pressed();		initOutput(); displayMin(18, true);			// 18 Minuten ausgeben
	 waitButton7Pressed();		displaySec(10, true);										// 10 Sekunden ausgeben
	 waitButton7Pressed();		displayMin(1, true);										// 1 Minuten ausgeben
	 waitButton7Pressed();		displayMin(5, false);										// Ausgabe Minuten aus
	 waitButton7Pressed();		displayMin(5, true);										// Ausgabe 5 Minuten
}

void testA5(void) {
	 Init_TI_Board();
	 timerinit();
	 initClock();
	
	// Stellen der Uhr
	waitButton7Pressed();	addStd(2);	addMin(3);	addSec(1);	displayClock(false);	// Ausgabe der Uhrzeit 02:03:01
	waitButton7Pressed();	addStd(1);							addSec(2);	displayClock(false);	// Ausgabe der Uhrzeit 03:03:03
	waitButton7Pressed();	addStd(21);													displayClock(false);	// Ausgabe der Uhrzeit 00:03:03

	// Teste Blinken der Uhr
	addStd(11);	addMin(1);	addSec(1);
	while (!readButton(BUTTON_PORT,PIN_SET_MODE)) { /* aktives Warten */ }
	while (readButton(BUTTON_PORT,PIN_SET_MODE)) {
		 displayClock(true);   											// Die Uhrzeit 11:04:04 muss mit der Frequenz von ca. 2,5 Hz blinken 
	}

	// Teste Gehen der Uhr
	addMin(55);
	addSec(45);
	while (!readButton(BUTTON_PORT,PIN_SET_MODE)) { /* aktives Warten */ }
	updateClock(true);
	while (readButton(BUTTON_PORT,PIN_SET_MODE)) {
		 updateClock(false);
		 displayClock(false);   											// Die Uhrzeit geht im Sekundentakt weiter. Bitte Wechsel der Stunde überprüfen
	}
	
	while (!readButton(BUTTON_PORT,PIN_SET_MODE)) { /* aktives Warten */ }
	updateClock(true);
	while (readButton(BUTTON_PORT,PIN_SET_MODE)) {
		 updateClock(true);
		 displayClock(false);   											// Die Uhrzeit ändert sich nicht
	}
	
	while (1) {
		 updateClock(false);
		 displayClock(false);   											// Die Uhr läuft normal weiter, sie darf am Anfang nicht springen
	}		
}

/* Implemtierung der Uhr -----------------------------------------------------*/



typedef enum{NORMAL_STATE, SET_HOUR_STATE, SET_MINUTES_STATE, SET_SECONDS_STATE} State;

/**
 * @brief  Die Funktion realisiert die Clock.
 *         Es wird gemäss DDC vorgegangen.
 */
void mainClock(void) {
    Init_TI_Board();
    initClock();
    initOutput();
    
    State state = NORMAL_STATE;
    bool s7_pressed = false;
    bool s6_pressed = false;
    
    while(true) {
        s7_pressed = readButton(BUTTON_PORT, 7);
        s6_pressed = readButton(BUTTON_PORT, 6);
        
        switch(state) {
            case NORMAL_STATE:
                if (s7_pressed) {
                    state = SET_HOUR_STATE;
                } else {
                    updateClock(false);
                    displayClock(false);
                }
            break;
            case SET_HOUR_STATE:
                updateClock(true);
                if (s7_pressed) {
                    state = SET_MINUTES_STATE;
                } else if(s6_pressed) {
                    addSec(1);
                }
                displayClock(true);
            break;
            case SET_MINUTES_STATE:
                if (s7_pressed) {
                    state = SET_SECONDS_STATE;
                } else if(s6_pressed) {
                    addMin(1);
                }
                displayClock(true);
            break;
            case SET_SECONDS_STATE:
                if (s7_pressed) {
                    state = NORMAL_STATE;
                } else if(s6_pressed) {
                    addSec(1);
                }
                displayClock(true);
            break;
        }
    }
}

/* main Funktion,  -----------------------------------------------------------*/

/**
 * @brief  Main program
 */
int main(void) {
	 // Rufe mainClock oder die Testfunktionen auf.
	 //testA3();
	 //testA4();
	 //testA5();

     mainClock();
	 return 0;
}
// EOF
