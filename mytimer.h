/**
 * @file mytimer.h
 * @author IHR NAME 
 * @date Dezember 2016
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es erg�nzt das Timer Modul des TI Board um eine Sleep Funktion.
 */
 
#ifndef _MYTIMER_H
#define _MYTIMER_H

#include <stdint.h>

/**
 * @brief  Diese Funktion berechnet die Zeitspanne zwischen zwei
 *         Zeitstempeln
 * @param  firstTimeStamp ist der erste (fr�here) Zeitstempel
 * @param  secondTimeStamp ist der zweite (sp�tere) Zeitstempel
 * @retval Die Zeit (in us), die zwischen den beiden Zeitstempeln vergangen ist.
 */
uint32_t timerDiffToUsec(uint32_t firstTimeStamp, uint32_t secondTimeStamp);

/**
 * @brief Diese Funktion wartet die vorgegebenen Zeitspannen.
 *        Sie ist durch polling der Timers realisiert.
 * @param t Die Zeit in us, die gewartet werden soll. Der Wertebereich des 
 *        Parameters ist 0 .. (UINT32_MAX-1)/TIM2_CLOCK
 */
void sleep(const uint32_t t);

#endif
// EOF

