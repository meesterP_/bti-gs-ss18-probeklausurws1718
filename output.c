/**
 * @file output.c
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es implementiert alle Ausgaben.
 */
	
#include <string.h>
#include <stdio.h>
#include "tft.h"
#include "general.h"
#include "myerr.h"
#include "hal.h"
#include "output.h"

/*
 ****************************************************************************************
 *  @brief Dies Funktion initialisiert das TFT Display.
 *         Auf dem TFT Display wird entweder eine Fenster f�r die Fehlermeldungen
 *         oder die Stunden als "gro�e" * dargestellt .
 *
 ****************************************************************************************/
void initOutput(void) {
    TFT_Init();
}

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt den aktuellen Fehlercode aus.
 *         Liegt kein Fehler vor, wird keine Ausgabe erzeugt.
 *
 ****************************************************************************************/
void displayError(void) {
    int error = getError();
    if (error != EOK) {
        char error_char = '0';
        TFT_cls();
        TFT_putc(error_char + error);
    }
}

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt die als Parameter �bergebene Anzahl der Stunden 
 *         auf dem TFT Display aus. Zuvor wir der Bildschirm gel�scht.
 *         F�r jede Stunde wird ein * ausgegeben. In einer Zeile stehen bis zu 5 Sternen.
 *  @param hrs        Anzahl der Stunden, die ausgegeben werden sollen.
 *  @param displayOn  Dieser Parameter legt fest, ob eine Ausgabe auf dem Bildschirm
                      erzeugt wird.
 *                    (false -> keine Zeichen auf dem Bildschirm)
 *
 ****************************************************************************************/
void displayHrs(unsigned char hrs, bool displayOn) {
    if (hrs > 23) {
        setError(IO_ERR);
        return;
    }
    
    TFT_cls();
    TFT_gotoxy(1,1);
    
    if (displayOn) {
        
        for (int i = 0; i < hrs; i++) {
            if (i+1%5 == 0) {
                TFT_newline();
                TFT_carriage_return();
            }
            TFT_putc('*');
        }
    }
    
}

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt die als Parameter �bergebene Anzahl der Minuten 
 *         auf den entsprechenden LEDs bin�r aus. 
 *  @param mins       Anzahl der Minuten, die ausgegeben werden sollen.
 *  @param displayOn  Dieser Parameter legt fest, ob eine Ausgabe auf den LEDs
                      erzeugt wird.
 *                    (false -> alle LEDs aus)
 *
 ****************************************************************************************/
void displayMin(unsigned char mins, bool displayOn) {
    if (mins > 59) {
        setError(IO_ERR);
        return;
    }
    
    if(!displayOn) {
        mins = 0;
    }
    
    setOutputOfPort(LED_PORT, LED_MINS_MASK, mins << 8);   
    
    /*GPIOG->BSRRH = GPIOG -> BSRRH & 0x00FF; // reset the LEDs
    
    if (displayOn) {
        GPIOG->BSRRL = GPIOG->BSRRL & (mins<<8 | 0x00FF);
    }*/
}

/*
 ****************************************************************************************
 *  @brief Diese Funktion gibt die als Parameter �bergebene Anzahl der Sekunden 
 *         auf den entsprechenden LEDs bin�r aus. 
 *  @param secs       Anzahl der Sekunden, die ausgegeben werden sollen.
 *  @param displayOn  Dieser Parameter legt fest, ob eine Ausgabe auf den LEDs
                      erzeugt wird.
 *                    (false -> alle LEDs aus)
 *
 ****************************************************************************************/
 void displaySec(unsigned char secs, bool displayOn) {
    if (secs > 59) {
        setError(IO_ERR);
        return;
    }
     
    if(!displayOn) {
         secs = 0;
    }
    setOutputOfPort(LED_PORT, LED_SECS_MASK, secs);  
    
    /*GPIOG->BSRRH = GPIOG -> BSRRH & 0xFF00; // reset the LEDs
    
    if (displayOn) {
        GPIOG->BSRRL = GPIOG->BSRRL & (secs<<8 | 0xFF00);
    }*/
}

// EOF
