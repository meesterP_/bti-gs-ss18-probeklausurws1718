/**
 * @file myerr.c
 * @author IHR NAME 
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es implementiert eine einfache Fehlerbehandlung.
 */

#include "myerr.h"

static char error = EOK;     // error speichert den zuletzt gemeldeten Fehler.

void setError(int errno){
	 error = errno;
}

int getError(void){
	 return error;
}

void resetError(void) {
	 error = EOK;
}

// EOF
