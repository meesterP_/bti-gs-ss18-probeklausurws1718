/**
 * @file hal.c
 * @author IHR NAME
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es implementiert den Zugriff auf die GPIOs.
 */

#include "hal.h"
#include "mytimer.h"
#include "myerr.h"

#define BOUNCE_DELAY   			(5*1000)     // us
#define NO_READ_ATTEMPTS		3            // Anzahl Leseversuche f�r die Verz�gerung beim Entprellen
#define MAX_PIN_OF_PORT			15           // Gr�esste gueltige Pin Nummer
  
/**
 * @brief Diese Funktion setzt mehrere nebeneinander liegende Pins eines
 *        Output Ports
 *
 * @param port Das Port, dessen Pins gesetzt werden sollen.
 * @param relevantPins In dieser Maske sind die Pins, die modifiziert 
 *        werden sollen, auf 1 gesetzt.
 * @param mask Diese Maske beschreibt die Werte der zu modifizierenden Pins. 
 */
void setOutputOfPort(GPIO_TypeDef* port, uint16_t relevantPins, uint16_t mask) {
    port->ODR = port->ODR & ~relevantPins;
    port->ODR = port->ODR & mask;
    
}

/**
 * @brief Diese Funktion liest einen Pin ein, an den ein Druckschalter
 *        angeschlossen ist. �ber einen einfachen Delay Mechanismus 
 *        wird eine softwaremaessige Entprellung realisiert.
 *        Im Fehlerfall wird der entsprechende Error Code gesetzt.
 * @param port Das Port, an dem der Druckschalter angeschlossen ist.
 * @param pin Nummer des Pins, an den der auszulesende Druckschalter angeschlossen ist.
 * @retval Die Funktion liefert true, genau dann wenn der Schalter gedrueckt ist.
 */
bool readButton(GPIO_TypeDef* port, unsigned char pin) {
    if (pin > MAX_PIN_OF_PORT) {
        setError(INTERNAL_ERR);
        return false;
    }
    for (int i = 0; i < NO_READ_ATTEMPTS; i++) {
        uint16_t first = port->IDR & 1<<pin;
        sleep(BOUNCE_DELAY);
        uint16_t second = port->IDR & 1<<pin;
        
        if (first == second) {
            if(first == 0) {
                return true;
            } else {
                return false;
            }
        }
    }
    setError(IO_ERR);
    return false;
}

// EOF
