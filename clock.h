/**
 * @file clock.h
 * @author IHR NAME
 * @date Dezember 2017
 * @brief Dieses Modul geh�rt zur L�sung der GS Probeklausur WS 2017-18. 
 *        Es stellt eine einfache Implementierung eines Uhr da.
 */
 
#ifndef _CLOCK_H
#define _CLOCK_H

#include <stdbool.h>

/**
 * @brief  Diese Funktion initialisiert die Uhr
 * @param  None
 * @retval None
 */
void initClock(void);

/**
 * @brief  Die Funktion addiert hrs Stunden auf die aktuelle Uhrzeit.
 * @param  hrs Anzahl der Stunden, die auf die Uhrzeit addiert werden.
 * @retval None
 */
void addStd(unsigned char hrs);

/**
 * @brief  Die Funktion addiert mins Minuten auf die aktuelle Uhrzeit.
 * @param  mins Anzahl der Minuten, die auf die Uhrzeit addiert werden.
 * @retval None
 */
void addMin(unsigned char mins);

/**
 * @brief  Die Funktion addiert secs Sekunden auf die aktuelle Uhrzeit.
 * @param  secs Anzahl der Sekunden, die auf die Uhrzeit addiert werden.
 * @retval None
 */
void addSec(unsigned char secs);

/**
 * @brief  Diese Funktion aktualisiert die Uhr.
 * @param  restart Dieser Parameter gibt an, die Uhr neu gestartet werden soll,
 *         nachdem sie �ber eine l�ngere Zeit nicht aktualisiert wurde.
 *         Dies sollte nach dem Stellen der Uhr durchgef�hrt werden. Ansonsten
 *         wird die Zeit zum Stellen der Uhr auf die Uhrzeit addiert.
 * @retval None
 */
 
void updateClock(bool restart);

/**
 * @brief  Diese Funktion gibt die Uhrzeit auf dem Display und den LEDs aus.
 * @param  blink Diese Parameter legt fest, ob die Darstellung der Uhrzeit blinken
 *         soll. Dies wird beim Einstellen der Uhr verwendet.
 * @retval none
 */

void displayClock(bool blink);

#endif
// EOF

